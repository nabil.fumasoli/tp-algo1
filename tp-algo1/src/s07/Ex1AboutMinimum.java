package s07;
import java.util.Arrays;

public class Ex1AboutMinimum {

  public static int min1(int[] t) {
    if (t.length > 0) {
      return singleRecursiveCall(t, 0);
    }
    return 0;

  }

  public static int min2(int[] t) {
    int mid = t.length / 2;
    if (mid < 1){
      return t[mid];
    }
    int minL = min2(Arrays.copyOfRange(t, 0, mid));
    int minR = min2(Arrays.copyOfRange(t, mid, t.length));
    if (minL < minR) {
      return minL;
    }
    else{
      return minR;
    }
  }

  //-------------------------------------------------------------------------

  private static int singleRecursiveCall(int[] t, int i) {
    if (i == t.length - 1)
      return t[i];
    int iRes = singleRecursiveCall(t, i+1);
    if (t[i] < iRes)
      return t[i];
    else
      return iRes;
  }


  @FunctionalInterface 
  interface MinFunction {
    int min(int[] t);
  }
  
  static void checkTestCase(int[] t, MinFunction mf) {
    int[] t1 = Arrays.copyOf(t, t.length);
    int observed = mf.min(t1);
    if(!Arrays.equals(t, t1)) 
      throw new IllegalStateException("The input array is modified!");
    int expected = Arrays.stream(t).min().getAsInt();
    if(observed != expected) 
      throw new IllegalStateException("Bad result: " + observed
          + " instead of " + expected + " in " + Arrays.toString(t));
  }
  
  private static void tinyMinTest() {
    int[][] samples = {
        {3, 4, 5},
        {5, 4, 3},
        {4, 3, 5},
        {-1, -9},
        {-9, -1},
        {8}
    };
    for(int[] u: samples) {
      checkTestCase(u, Ex1AboutMinimum::min1);
      checkTestCase(u, Ex1AboutMinimum::min2);
    }
    System.out.print("End of tiny test.");
  }

  public static void main(String [] args) {
    int[] t = {4, 3, 2, 6, 8, 7};
    System.out.println(min1(t));
    System.out.println(min2(t));
    tinyMinTest();
  }

}
