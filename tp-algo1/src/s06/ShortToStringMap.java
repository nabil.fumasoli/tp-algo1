package s06;

import java.util.Arrays;

public class ShortToStringMap {
    private int size;
    private short[] keys;
    private String[] values;

    // TODO ...
    //------------------------------
    //  Private methods
    //------------------------------

    // Could be useful, for instance :
    // - one method to detect and handle the "array is full" situation
    // - one method to locate a key in the array
    //   (to be called from containsKey(), put(), and remove())

    //------------------------------
    //  Public methods
    //------------------------------
    public ShortToStringMap() {
        final int MAX_VAL = 30;
        this.keys = new short[MAX_VAL];
        this.values = new String[MAX_VAL];

    }

    // adds an entry in the map, or updates it
    public void put(short key, String img) {
        int u = findKey(key);
        if (u != -1)
            values[u] = img;
        else {
            if (keys.length == size())
                upgradeSizes();
            values[size] = img;
            keys[size] = key;
            size++;
        }

    }

    // returns null if !containsKey(key)
    public String get(short key) {
        String u;
        try {
            u = values[findKey(key)];
        } catch (ArrayIndexOutOfBoundsException e) {
            u = null;
        }
        return u;
    }

    public void remove(short e) {
        int indexK = findKey(e);
        if (keys.length == size())
            upgradeSizes();
        if (indexK > -1) {
            keys[indexK] = keys[size - 1];
            values[indexK] = values[size - 1];
            size--;
        }
    }

    public boolean containsKey(short k) {
        return findKey(k) > -1;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return size;
    }

    public ShortToStringMapItr iterator() {
        return new ShortToStringMapItr() {
            private int i = 0;

            @Override
            public boolean hasMoreKeys() {
                return i < size;
            }

            @Override
            public short nextKey() {
                if (!hasMoreKeys())
                    return -1;
                return keys[i++];
            }
        };
    }

    // a.union(b) :        a becomes "a union b"
    //  values are those in b whenever possible
    public void union(ShortToStringMap m) {
        for (short i = 0; i < m.size(); ++i)
            this.put(m.keys[i], m.values[i]);
    }

    // a.intersection(b) : "a becomes a intersection b"
    //  values are those in b
    public void intersection(ShortToStringMap s) {
        for (short i = 0; i < this.size; i++) {
            if (!s.containsKey(this.keys[i])) {
                this.remove(this.keys[i]);
                i = -1;
            } else {
                this.put(this.keys[i], s.get(this.keys[i]));
            }
        }
    }

    // a.toString() returns all elements in
    // a string like: {3:"abc",9:"xy",-5:"jk"}
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("{");
        for (int i = 0; i < size; i++) {
            str.append(keys[i] + ":\"" + values[i] + "\" ");
        }
        str.append("}");
        return str.toString();
    }


    //----------------------------------
    //  Private methods - personal use
    //----------------------------------

    //method used to find a key in the keys
    private int findKey(short key) {
        for (int i = 0; i < size; ++i) {
            if (keys[i] == key)
                return i;
        }
        return -1;
    }

    //Method used to upgrade the tables
    private void upgradeSizes() {
        keys = Arrays.copyOf(keys, size * 2);
        values = Arrays.copyOf(values, size * 2);
    }

}
