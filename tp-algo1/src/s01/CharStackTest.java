package s01;

public class CharStackTest {
    public static void main(String[] args) {
        CharStack v = new CharStack();
        CharStack b = new CharStack(4);

        System.out.println("test v.isEmpty : " + v.isEmpty());

        System.out.println("Test v.pop when empty : ");
        try {
            v.pop();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        System.out.println("v.top when empty : ");
        try {
            v.top();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        System.out.println("v.pop with 5 values : ");
        v.push('a');
        try {
            System.out.println(v.top());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }        v.push('b');
        v.push('c');
        try {
            System.out.println(v.top());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        v.push('d');
        try {
            System.out.println(v.top());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        v.push('e');
        try {
            System.out.println(v.top());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            System.out.println(v.pop());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println("v.push after a pop");
        v.push('f');
        try {
            System.out.println(v.top());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        //check debugger because array should have double the length
        System.out.println("b.push 5 times : ");
        b.push('r');
        b.push('s');
        b.push('t');
        b.push('u');
        b.push('v');
        try {
            System.out.println(b.top());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
