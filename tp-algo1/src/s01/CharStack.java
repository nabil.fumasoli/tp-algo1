package s01;

public class CharStack {
  private int    topIndex;
  private char[] buffer;

  private static final int DEFAULT_SIZE = 10;

  /**
   * Constructor used to create a CharStack with a default size
   */
  public CharStack() {
    this(DEFAULT_SIZE);

  }

  /**
   * Constructor used to a CharStack with a certain size given in
   * parameter. Creates a char array and put the index at -1, -1 means it is empty.
   * @param estimatedSize int used for the size of the array.
   */
  public CharStack(int estimatedSize) {
    this.buffer = new char[estimatedSize];
    this.topIndex = -1;
  }

  /**
   * Method used to check if the array is empty with the position of the index.
   * @return false if it contains something, true if empty
   */
  public boolean isEmpty() {
    return this.topIndex < 0;
  }

  /**
   * Method used to get the element at the top of the stack
   * @return a char at the top
   * @throws Exception throws an exception if the stack is empty
   */
  public char top() throws Exception {
    if (isEmpty())
      throw new Exception("top() -- Stack Empty");
    return this.buffer[topIndex];
  }

  /**
   * Method used to remove the element at the top of the stack
   * @return the char poped
   * @throws Exception throws an exception if the stack is empty
   */
  public char pop() throws Exception {
    if (isEmpty())
      throw new Exception("pop() -- Stack Empty");
    char u = this.buffer[topIndex];
    this.buffer[topIndex] = '\0';
    this.topIndex--;
    return u;
  }

  /**
   * Method used to add an element at the top of the stack
   * @param x the char to add
   */
  public void push(char x) {
    if (this.topIndex == (this.buffer.length-1))
      doubleSizeArray();
    ++this.topIndex;
    this.buffer[topIndex] = x;
  }

  private void doubleSizeArray(){
    char[] u = new char[(this.buffer.length*2)];
    for (int i = 0; i <= this.topIndex; i++) {
      u[i] = this.buffer[i];
    }
    this.buffer = u;
  }

}
