package s01;

public class CharStackN {
  private int    topIndex;
  private char[] buffer;

  private static final int DEFAULT_SIZE = 10;

  public CharStackN() {
    this(DEFAULT_SIZE);
  }

    public CharStackN(int estimatedSize) {
        char[] charStack = new char[estimatedSize];
        if (topIndex == estimatedSize - 1) {
            charStack = new char[estimatedSize * 2];
        }
        buffer = charStack;
  } 

  public boolean isEmpty() {
      return topIndex == -1; // TODO
  } 

  public char top() {
      return buffer[topIndex]; // TODO
  }

    public char pop() {
        char c = buffer[topIndex];
        buffer[topIndex] = '\0';
        topIndex--;
        return c; // TODO
    }

    public void push(char x) {
        // TODO
        ++topIndex;
        buffer[topIndex] = x;
    }

}
