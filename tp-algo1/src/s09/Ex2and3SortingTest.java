package s09;

import java.util.Arrays;

public class Ex2and3SortingTest {
    //----- Maybe you'll find that useful for ex. (3)... -------
    @FunctionalInterface
    interface ISorting {
        void sort(int[] t);
    }

    static final ISorting[] algos = {
            BuggySorting::sort00, BuggySorting::sort01,
            BuggySorting::sort02, BuggySorting::sort03,
            BuggySorting::sort04, BuggySorting::sort05,
            BuggySorting::sort06, BuggySorting::sort07,
            BuggySorting::sort08, BuggySorting::sort09,
            BuggySorting::sort10, BuggySorting::sort11,
            Arrays::sort
    };

    // for(ISorting a:algos) {
    //   ...
    //   a.sort(t);
    //   ...
    // }

    //=============================================================
    private static boolean arrayIsSorted_Kais(int arr[], int n) {
        //is the array too small (empty or 1 elt)
        if (n == 0 || n == 1)
            return true;
        //Start at 2nd elt
        for (int i = 1; i < n; i++) {
            // Not sorted?
            if (arr[i-1] > arr[i])
                return false;
        }
        // is Ok
        return true;
    }

    //JE METS CA LA POUR LA REVOIR CAR MEILLEUR QUE LA MIENNE, PEUT ÊTRE IGNORER
    private static int arraySortedOrNot_GeeksForGeek(int arr[], int n) {
        // Array has one or no element or the
        // rest are already checked and approved.
        if (n <= 1)
            return 1;
        // Unsorted pair found (Equal values allowed)
        if (arr[n - 1] < arr[n - 2])
            return 0;
        // Last pair was sorted
        // Keep on checking
        return arraySortedOrNot_GeeksForGeek(arr, n - 1);
    }

    public static boolean isSortingResultCorrect(int[] orig, int[] sorted) {
        if (orig.length != sorted.length)
            return false;
        for (int i = 0; i < sorted.length; i++) {
            // test if nbOfOccurrences orig is equal to nbOfOccurrences sorted
            if (nbOfOccurrences(sorted, sorted[i]) != nbOfOccurrences(orig, orig[i]))
                return false;
            // test of monotonic datas
            if (!arrayIsSorted_Kais(sorted, sorted.length))
                return false;
        }
        return true;
    }

    // Maybe you'll find such a method useful...
    private static int nbOfOccurrences(int[] t, int e) {
        int nbOfOccurrences = 0;
        for (int i = 0; i < t.length - 1; i++) {
            if (t[i] == e)
                nbOfOccurrences++;
        }
        return nbOfOccurrences;
    }

    // ------------------------------------------------------------
    public static void main(String[] args) {
        /*
        *   1) highest to lowest with 20 cases
        *   2) size of expected is not respected
        *   3) unsorted to sorted "basic version"
        *   4) only 0's, same elts
        *   5) already sorted to sorted
        *   6) every elts different
        *   7) empty
        */
        int[][] origin = {
                {20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1},
                {1, 2, 3, 4},
                {2, 5, 4, 1, 3, 6, 7, 9, 8},
                {0, 0, 0, 0},
                {1, 2, 3, 4, 5, 6, 7},
                {8, 7, 6, 5, 4, 3, 2, 1},
                {}
        };
        int[][] sorted = {
                {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20},
                {1, 2, 3},
                {1, 2, 3, 4, 5, 6, 7, 8, 9},
                {0, 0, 0, 0},
                {1, 2, 3, 4, 5, 6, 7},
                {1, 2, 3, 4, 5, 6, 7, 8},
                {}
        };
        int sortAlgoNumber = 0;
        for (int i = 0; i < origin.length - 1; i++) {
            System.out.println("Array number: " + i);
            for (ISorting a : algos) {
                int[] origToSort = Arrays.copyOf(origin[i], origin[i].length);
                a.sort(origToSort);
                System.out.println(" Sorting n." + (++sortAlgoNumber) + " is: " + isSortingResultCorrect(origToSort, sorted[i]));
            }
            sortAlgoNumber = 0;
        }
    }


}
