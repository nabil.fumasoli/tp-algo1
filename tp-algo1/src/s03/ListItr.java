package s03;
public class ListItr {
  final List list;
  ListNode pred, succ;
  // ----------------------------------------------------------
  public ListItr( List anyList ) {
    list = anyList; 
    goToFirst();   
  }

  public void insertAfter(int e) {
    ListNode ln = new ListNode(e, pred, succ);
    if (list.isEmpty()) {
      list.first = ln;
      list.last = ln;
    } else if (isFirst()) {
      succ.prev = ln;
      list.first = ln;
    } else if (isLast()) {
      pred.next = ln;
      list.last = ln;
    } else {
      pred.next = ln;
      succ.prev = ln;
    }
    list.size++;
    succ = ln;
  }

  public void removeAfter() {
    succ = succ.next;
    if (list.size() == 1) {
      list.first = null;
      list.last = null;
    } else if (isFirst()) {
      succ.prev = null;
      list.first = succ;
    } else if (isLast()) {
      pred.next = null;
      list.last = pred;
    } else {
      pred.next = succ;
      succ.prev = pred;
    }
    list.size--;

  }

  public int  consultAfter() {
    return succ.elt;
  }
  public void goToNext() {
    pred = succ; 
    succ = succ.next; 
  }
  public void goToPrev() {
    succ = pred;
    pred = pred.prev; 
  }
  public void goToFirst() { 
    succ = list.first; 
    pred = null;
  }
  public void goToLast() { 
    pred = list.last;  
    succ = null;
  }
  public boolean isFirst() { 
    return pred == null;
  }
  public boolean isLast() { 
    return succ == null; 
  }
}

// When isFirst(), it is forbidden to call goToPrev()
// When isLast(),  it is forbidden to call goToNext() 
// When isLast(),  it is forbidden to call consultAfter(), or removeAfter()
// For an empty list, isLast()==isFirst()==true
// For a fresh ListItr, isFirst()==true
// Using multiple iterators on the same list is allowed only 
//   if none of them modifies the list
