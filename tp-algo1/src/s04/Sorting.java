package s04;

import s03.ListItr;

public class Sorting {
  public static void main(String [] args) {
    int[] t = {4, 3, 2, 6, 8, 7};
    int[] u = {2, 3, 4, 6, 7, 8};
    selectionSort(t);
    for (int i=0; i<t.length; i++) 
      if(t[i] != u[i]) {
        System.out.println("Something is wrong..."); return;
      }
    int[] v = {5};
    selectionSort(v);
    int[] w = {};
    selectionSort(w);
    System.out.println("\nMini-test passed successfully...");
  }
  //------------------------------------------------------------
  public static void selectionSort(int[] a) {
    int position, swap;
    for (int i = 0; i < a.length; i++) {
      swap = a[i];
      position = i;
      for (int j = i + 1; j < a.length; j++) {
        if (swap > a[j]) {
          swap = a[j];
          position = j;
        }
      }
      a[position] = a[i];
      a[i] = swap;
    }
  }
  //------------------------------------------------------------
  public static void shellSort(int[] a) {
    int n = a.length;
    for (int gap = n / 2; gap > 0; gap /= 2) {
      for (int i = gap; i < n; i++) {
        int key = a[i];
        int j = i;
        while (j >= gap && a[j - gap] > key) {
          a[j] = a[j - gap];
          j -= gap;
        }
        a[j] = key;
      }
    }
  }
  //------------------------------------------------------------
  public static void insertionSort(int[] a) {
    int i, j, v;

    for (i=1; i<a.length; i++) {
      v = a[i];          // v is the element to insert
      j = i;
      while (j>0 && a[j-1] > v) {
        a[j] = a[j-1];   // move to the right
        j--;
      }
      a[j] = v;          // insert the element
    }
  }
  //------------------------------------------------------------
}
